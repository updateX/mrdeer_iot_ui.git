// 统一请求路径前缀在libs/axios.js中修改
import {getRequest, postBodyRequest, postRequest} from "@/libs/axios";

export const driverApi = {

    addDriver: (params) => postBodyRequest('/iot/manager/driver/add', params),

    deleteDriver: (id) => postRequest(`/iot/manager/driver/delete/${id}`, id),

    getDriverByHostAndPort: (host, port) => getRequest(`/iot/manager/driver/host/${host}/port/${port}`, null),

    getDriverById: (id) => getRequest(`/iot/manager/driver/id/${id}`, id),

    listDriver: (params) => postBodyRequest(`/iot/manager/driver/list`, params),

    getDriverByServiceName: (serviceName) => getRequest(`/iot/manager/driver/service/${serviceName}`, serviceName),

    getDriverStatus: (params) => postBodyRequest('/iot/manager/driver/status', params),

    updateDriver: (params) => postBodyRequest('/iot/manager/driver/update', params)
}
