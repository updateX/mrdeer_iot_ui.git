// 统一请求路径前缀在libs/axios.js中修改

// 高德地图key
import {getRequest, postBodyRequest, postRequest} from "../libs/axios";

export const amapKey = ""
// 文件上传接口
export const uploadFile = "/mrdeer/upload/file"
//----------------------------店铺管理------------------------------------
export const addShop = (params) => {
    return postBodyRequest('/shop/add', params);
}

export const deleteShopByIds = (ids) => {
    return postRequest(`/shop/delByIds/`, ids);
}

export const updateShop = (shopEntity) => {
    return postBodyRequest(`/shop/edit`, shopEntity);
}

export const getAll = (params) => {
    return getRequest('/shop/getAll', params);
}

export const getShopList = (params) => {
    return postBodyRequest('/shop/list', params);
}
//----------------------------设备管理------------------------------------
export const addDevice = (params) => {
    return postBodyRequest('/device/addDevice', params);
}

export const deleteDeviceById = (id) => {
    return postRequest(`/device/deleteDevice/${id}`);
}

export const updateDevice = (deviceEntity) => {
    return postBodyRequest(`/device/updateDevice`, deviceEntity);
}

export const getDeviceById = (id) => {
    return getRequest('/device/getDeviceById/${id}');
}

export const getDeviceList = (params) => {
    return postBodyRequest('/device/listDevice', params);
}
//----------------------------整机指令------------------------------------
export const setStart = (address, flag) => {
    return postRequest(`/command/wash/start/${address}/${flag}`);
}
export const setAuth = (address, flag) => {
    return postRequest(`/command/wash/authorize/${address}/${flag}`);
}
export const setMute = (address, flag) => {
    return postRequest(`/command/all/set/mute/${address}/${flag}`);
}
export const setDoor = (address, flag) => {
    return postRequest(`/command/wash/door/${address}/${flag}`);
}
export const setProgram = (address, type) => {
    return postRequest(`/command/wash/program/${address}/${type}`);
}
export const stop = (address) => {
    return postRequest(`/command/wash/stop/${address}`);
}

export const qrcode = (content) => {
    return getRequest(`/qrcode/getqr/logo/code/${content}`)
}