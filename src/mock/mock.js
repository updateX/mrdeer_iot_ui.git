import Mock from 'mockjs'

Mock.mock('/login', {
    'name': '@name'
});

Mock.mock('/list', {    //输出数据
    'name': '@name',    //随机生成姓名
    'age|10-20': 10
    //还可以定义其他数据
});

